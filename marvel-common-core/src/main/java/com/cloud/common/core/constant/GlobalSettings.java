package com.cloud.common.core.constant;

/**
 * @Name GlobalSettings
 * @Description TODO
 * @Author douhaichao
 * @Date 2019/3/14 14:44
 * @Version 1.0
 **/
public class GlobalSettings {
    public final static String Send_Sms_Code_Mobile_ValidateCode_Content = "验证码：%s,%s分钟内有效,%s";

    //总部
    public final static String ZB_COOKIE_NAME = "zb_cookie_name";
    public final static String WWW_COOKIE_NAME = "www_cookie_name";
    public final static String ZBIMGCode_COOKIE_NAME = "zbimgcode_cookie_name";
    public final static String WWWIMGCode_COOKIE_NAME = "wwwimgcode_cookie_name";
    public final static String H5_COOKIE_NAME = "h5_cookie_name";
    public final static String H5IMGCode_COOKIE_NAME = "h5imgcode_cookie_name";

    //服务中心
    public final static String THOR_COOKIE_NAME = "thor_cookie_name";
    public final static String THOR_REDIS_MENU = "thor_redis_menu";
    public final static String THOR_ALL_REDIS_MENU = "thor_all_redis_menu";
    public final static String THORIMGCode_COOKIE_NAME = "thorimgcode_cookie_name";

    //商户中心
    public final static String HULK_COOKIE_NAME = "hulk_cookie_name";
    public final static String HULK_REDIS_MENU = "hulk_redis_menu";
    public final static String HULK_ALL_REDIS_MENU = "hulk_all_redis_menu";
    public final static String HULKIMGCode_COOKIE_NAME = "hulkimgcode_cookie_name";

    //积分商城
    public final static String LOKI_COOKIE_NAME = "loki_cookie_name";
    public final static String LOKI_REDIS_MENU = "loki_redis_menu";
    public final static String LOKI_ALL_REDIS_MENU = "loki_all_redis_menu";
    public final static String LOKIIMGCode_COOKIE_NAME = "lokiimgcode_cookie_name";
    public final static String LOKI_BID_COOKIE_NAME = "bid";
    public final static String LOKI_BNAME_COOKIE_NAME = "bname";
    public final static String LOKI_SEARCH_HISTORY_NAME = "loki_search_history_name";

    public final static String AES_KEY_COOKIE = "k;)*(+nmjdsf$#@d";
    public final static String SELLIMGCode_COOKIE_NAME = "sellimgcode_cookie_name";
    public final static String WEBIMGCode_COOKIE_NAME = "webimgcode_cookie_name";
}
