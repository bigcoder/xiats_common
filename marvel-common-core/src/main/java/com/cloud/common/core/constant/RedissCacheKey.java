package com.cloud.common.core.constant;

/**
 * @Name RedissCacheKey
 * @Description TODO
 * @Author douhaichao
 * @Date 2019/3/15 10:23
 * @Version 1.0
 **/
public class RedissCacheKey {
    /**
     * 测试一年龄计分
     */
    public static final String CP_AGE = "base_cp_age";
    /**
     * 测评职业锚计分
     */
    public static final String CP_ANCHOR = "base_cp_anchor";
    /**
     * 测评情商计分
     */
    public static final String CP_EQ = "base_cp_eq";
    /**
     * 测评属性
     */
    public static final String CP_DESCRIPTION = "base_cp_description";
    /**
     * 测评结果主题
     */
    public static final String CP_ONE = "base_cp_one";
    /**
     * 测评八人格计分
     */
    public static final String CP_PERSONALITY = "base_cp_personality";

    /**
     * 能力与工作对应关系
     */
    public static final String CP_ABILITY_WORK = "base_cp_ability_work";

    /**
     * 测评主题属性分数表
     */
    public static final String CP_SCORE = "base_cp_score";
    /**
     * 分组维护
     */
    public static final String DROPDOWN = "base_dropdown";
    /**
     * 测评列表
     */
    public static final String PROJECT = "c_project";
    /**
     * 测评题
     */
    public static final String PROJECT_QUESTION = "c_project_question";
    /**
     * 滚动图
     */
    public static final String SCROLL_CHART = "c_scroll_chart";


    /**
     * 首页已完成测评人
     */
    public static final String Home_Complete_Cp_User="home_complete_cp_user";

    /**
     * 首页已完成支付测评人
     */
    public static final String Home_Complete_Payment_User="home_complete_payment_user";

    /**
     * 年级标准分计算
     */
    public static final String CP_CLASS="base_cp_class";

    /**
     * 推荐职业与所学专业的对应关系
     */
    public static final String CP_MAJOR_WORK="base_cp_major_work";

    /**
     * 权重分配
     */
    public static final String CP_WEIGHT_DISTRIBUTION="base_cp_weight_distribution";

    /**
     * 省
     */
    public static final String SYS_PROVINCE="sys_province";

    /**
     * 市
     */
    public static final String SYS_CITY="sys_city";

    /**
     * 区
     */
    public static final String SYS_AREA="sys_area";
}
