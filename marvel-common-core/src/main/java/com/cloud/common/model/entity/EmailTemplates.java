package com.cloud.common.model.entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigInteger;
import java.util.Date;

/**
 * 邮箱模版
 *
 * @author douhaichao code generator
 * @date 2021-01-19 21:19:43
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class EmailTemplates {
    /**
     * 别名
     */
    private String alias;

    /**
     * 名称
     */
    private String name;

    /**
     * 主题
     */
    private String subject;

    /**
     * 模版
     */
    private String value;

    /**
     * 发送方式
     */
    private String way;

    /**
     * 说明
     */
    private String params;

    /**
     * 模块
     */
    private String module;

    /**
     * 邮箱类型
     */
    private Integer type;

    /**
     * 开关
     */
    private Integer isSwitch;

}
