package com.cloud.common.model.entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 短信模版
 *
 * @author douhaichao code generator
 * @date 2021-01-29 21:46:33
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class SmsTemplates {

    /**
     * 别名
     */
    private String alias;

    /**
     * 名称
     */
    private String name;

    /**
     * 模版
     */
    private String value;

    /**
     * 模版ID
     */
    private String tempId;

    /**
     * 发送方式
     */
    private String way;

    /**
     * 说明
     */
    private String params;

    /**
     * 模块
     */
    private String module;

    /**
     * 短信类型1:验证码，2:通知，3:营销
     */
    private Integer type;

    /**
     * 开关
     */
    private Integer isSwitch;

}
