package com.cloud.common.model.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
//货币类型
public enum CurrencyTypeEnum {
    CASH(1,"现金"),
    CANDY(2,"喜糖"),
    ;
    private int code;
    private String desc;

    CurrencyTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static CurrencyTypeEnum getEnumDesc(String desc) {
        for (CurrencyTypeEnum c : CurrencyTypeEnum.values()) {
            if (c.getDesc().equals(desc)) {
                return c;
            }
        }
        return null;
    }

    public static CurrencyTypeEnum getEnumCode(String code) {
        for (CurrencyTypeEnum c : CurrencyTypeEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static List<Map<String, String>> getAllEnum() {
        List<Map<String, String>> list = new ArrayList<>();
        for (CurrencyTypeEnum c : CurrencyTypeEnum.values()) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("value", c.getCode() + "");
            map.put("label", c.getDesc());
            list.add(map);
        }
        return list;
    }
}
