package com.cloud.common.model.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum MailTemplatesEnum {
    REG_CODE("reg_code","注册验证码"),
    LOGIN_CODE("login_code","登录验证码"),
    EMAIL_EDIT_CODE("email_edit_code","修改邮箱验证码"),
    REGISTER("register","注册成功提醒"),
    LOGIN("login","登录成功提醒"),
    SINGLE_PHOTO_CHECK_SUCCESS("single_photo_check_success","单身照片通过审核"),
    SINGLE_PHOTO_CHECK_FAIL("single_photo_check_fail","单身照片未通过"),
    SINGLE_CHECK_SUCCESS("single_check_success","单身资料通过审核"),
    SINGLE_CHECK_FAIL("single_check_fail","单身资料通过未审核"),
    IDENTITY_CHECK_SUCCESS("identity_check_success","身份认证通过"),
    IDENTITY_CHECK_FAIL("identity_check_fail","身份认证未通过"),
    EDUCATION_CHECK_SUCCESS("education_check_success","学历认证审核通过"),
    EDUCATION_CHECK_FAIL("education_check_fail","学历认证审核不通过"),
    WORK_CHECK_SUCCESS("work_check_success","工作认证审核通过"),
    WORK_CHECK_FAIL("work_check_fail","工作认证审核不通过"),
    HAUSE_CHECK_SUCCESS("hause_check_success","房产认证审核通过"),
    HAUSE_CHECK_FAIL("hause_check_fail","房产认证审核不通过"),
    CAR_CHECK_SUCCESS("car_check_success","工作认证审核通过"),
    CAR_CHECK_FAIL("car_check_fail","工作认证审核不通过"),
    WITHDRAWAL_CHECK_SUCCESS("withdrawal_check_success","提现审核通过"),
    WITHDRAWAL_CHECK_FAIL("withdrawal_check_fail","提现审核不通过"),
    PAYMENT_ORDER_SUCCESS("payment_order_success","订单支付成功"),
    SETMEAL_EXPIRE("setmeal_expire","套餐到期提醒"),
    USER_OAUTH_BIND("user_oauth_bind","第三方账号绑定提醒"),
    USER_OAUTH_UNBIND("user_oauth_unbind","第三方账号解除绑定提醒"),
    MOBILE_BIND("mobile_bind","绑定手机提醒"),
    FRIEND_APPLY("friend_apply","好友申请提醒"),
    FRIEND_APPLY_AGREE("friend_apply_agree","对方同意了好友申请"),
    FRIEND_APPLY_REFUSE("friend_apply_refuse","对方拒绝了好友申请"),
    CONTACT_APPLY("contact_apply","联系方式申请提醒"),
    CONTACT_APPLY_AGREE("contact_apply_agree","对方同意了联系方式申请"),
    CONTACT_APPLY_REFUSE("contact_apply_refuse","对方拒绝了联系方式申请"),
    CHAT_REMIND("chat_remind","聊天提醒"),
    GREET_REMIND("greet_remind","打招呼提醒"),
    LIKE_REMIND("like_remind","心动提醒"),
    FOOTPRINT_REMIND("footprint_remind","被查看资料提醒"),
    NOTICE("notice","系统消息"),
    ;
    private String title;
    private String desc;

    MailTemplatesEnum(String title, String desc) {
        this.title = title;
        this.desc = desc;
    }

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }

    public static MailTemplatesEnum getEnumDesc(String desc) {
        for (MailTemplatesEnum c : MailTemplatesEnum.values()) {
            if (c.getDesc().equals(desc)) {
                return c;
            }
        }
        return null;
    }

    public static MailTemplatesEnum getEnumTitle(String title) {
        for (MailTemplatesEnum c : MailTemplatesEnum.values()) {
            if (c.getTitle().equals(title)) {
                return c;
            }
        }
        return null;
    }

    public static List<Map<String, String>> getAllEnum() {
        List<Map<String, String>> list = new ArrayList<>();
        for (MailTemplatesEnum c : MailTemplatesEnum.values()) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("value", c.getTitle() + "");
            map.put("label", c.getDesc());
            list.add(map);
        }
        return list;
    }
}
