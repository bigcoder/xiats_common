package com.cloud.common.model.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum MessageTypeEnum {
    MOBILE(1,"手机"),
    EMAIL(2,"邮箱"),
    ;
    private int code;
    private String desc;

    MessageTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static MessageTypeEnum getEnumDesc(String desc) {
        for (MessageTypeEnum c : MessageTypeEnum.values()) {
            if (c.getDesc().equals(desc)) {
                return c;
            }
        }
        return null;
    }

    public static MessageTypeEnum getEnumCode(String code) {
        for (MessageTypeEnum c : MessageTypeEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static List<Map<String, String>> getAllEnum() {
        List<Map<String, String>> list = new ArrayList<>();
        for (MessageTypeEnum c : MessageTypeEnum.values()) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("value", c.getCode() + "");
            map.put("label", c.getDesc());
            list.add(map);
        }
        return list;
    }
}
