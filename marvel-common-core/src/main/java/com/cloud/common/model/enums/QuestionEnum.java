package com.cloud.common.model.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum QuestionEnum {
    E_LBTLNL(1, "类比推理能力-企业版",1),
    E_YYLJYBDNL(2, "言语理解与表达能力-企业版",1),
    E_SXNL(3, "数学能力-企业版",1),
    E_JYL(4, "记忆力-企业版",2),
    E_ZHL(5, "智慧力-企业版",1),
    E_ZYMCY(6, "职业锚测验-企业版",1),
    E_QSCY(7, "情商测验-企业版",1),
    E_RGCS(8, "人格测试-企业版",1),
    S_DYZNCST(9, "多元智能测试题-学生版",1),
    S_YYLJYBDNL(10, "言语理解与表达能力-学生版",3),
    S_SXNLCY(11, "数学能力测验-学生版",1),
    ;
    private int code;
    private String desc;
    /**
     * 分组   1选择题 2顺序题 3组词题
     */
    private int group;

    QuestionEnum(int code, String desc,int group) {
        this.code = code;
        this.desc = desc;
        this.group = group;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public int getGroup() {
        return group;
    }

    public static QuestionEnum getEnum(String desc) {
        for (QuestionEnum c : QuestionEnum.values()) {
            if (c.getDesc().equals(desc)) {
                return c;
            }
        }
        return null;
    }

    public static QuestionEnum getEnum(int index) {
        for (QuestionEnum c : QuestionEnum.values()) {
            if (c.getCode() == index) {
                return c;
            }
        }
        return null;
    }

    public static List<Map<String, String>> getAllEnum() {
        List<Map<String, String>> list = new ArrayList<>();
        for (QuestionEnum c : QuestionEnum.values()) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("value", c.getCode() + "");
            map.put("label", c.getDesc());
            map.put("group", c.getGroup()+"");
            list.add(map);
        }
        return list;
    }
}
