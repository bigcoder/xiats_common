package com.cloud.common.model.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum QuestionTypeEnum {
    ENTERPRISE(1,"企业版"),
    STUDENT(2,"学生版"),
    SOCIOLOGY(3,"社会版"),
    ;
    private int code;
    private String desc;

    QuestionTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static QuestionTypeEnum getEnumDesc(String desc) {
        for (QuestionTypeEnum c : QuestionTypeEnum.values()) {
            if (c.getDesc().equals(desc)) {
                return c;
            }
        }
        return null;
    }

    public static QuestionTypeEnum getEnumCode(String code) {
        for (QuestionTypeEnum c : QuestionTypeEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static List<Map<String, String>> getAllEnum() {
        List<Map<String, String>> list = new ArrayList<>();
        for (QuestionTypeEnum c : QuestionTypeEnum.values()) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("value", c.getCode() + "");
            map.put("label", c.getDesc());
            list.add(map);
        }
        return list;
    }
}
