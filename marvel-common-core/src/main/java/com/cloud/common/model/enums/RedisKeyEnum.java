package com.cloud.common.model.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum RedisKeyEnum {
    bcSingleListCategoryRedisKey("bc:single:category:all", "单身分类"),
    bcConfigRedisKey("bc:config:all", "系统配置"),
    bcCategoryDistrictsCache("bc:categorydistricts:all", "省市区"),
    bcEmailTemplateRedisKey("bc:emailtemplate:all", "邮件模板"),
    bcSmsTemplateRedisKey("bc:smstemplate:all", "手机短信模板"),
    ;
    private String title;
    private String desc;

    RedisKeyEnum(String title, String desc) {
        this.title = title;
        this.desc = desc;
    }

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }

    public static RedisKeyEnum getEnumDesc(String desc) {
        for (RedisKeyEnum c : RedisKeyEnum.values()) {
            if (c.getDesc().equals(desc)) {
                return c;
            }
        }
        return null;
    }

    public static RedisKeyEnum getEnumTitle(String title) {
        for (RedisKeyEnum c : RedisKeyEnum.values()) {
            if (c.getTitle().equals(title)) {
                return c;
            }
        }
        return null;
    }

    public static List<Map<String, String>> getAllEnum() {
        List<Map<String, String>> list = new ArrayList<>();
        for (RedisKeyEnum c : RedisKeyEnum.values()) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("value", c.getTitle() + "");
            map.put("label", c.getDesc());
            list.add(map);
        }
        return list;
    }
}
