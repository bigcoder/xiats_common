package com.cloud.common.model.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum SmsTemplatesEnum {
    REG_CODE("reg_code","注册验证码"),
    LOGIN_CODE("login_code","登录验证码"),
    MOBIIE_EDIT_CODE("mobiie_edit_code","修改手机号验证码"),
    ;
    private String title;
    private String desc;

    SmsTemplatesEnum(String title, String desc) {
        this.title = title;
        this.desc = desc;
    }

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }

    public static SmsTemplatesEnum getEnumDesc(String desc) {
        for (SmsTemplatesEnum c : SmsTemplatesEnum.values()) {
            if (c.getDesc().equals(desc)) {
                return c;
            }
        }
        return null;
    }

    public static SmsTemplatesEnum getEnumTitle(String title) {
        for (SmsTemplatesEnum c : SmsTemplatesEnum.values()) {
            if (c.getTitle().equals(title)) {
                return c;
            }
        }
        return null;
    }

    public static List<Map<String, String>> getAllEnum() {
        List<Map<String, String>> list = new ArrayList<>();
        for (SmsTemplatesEnum c : SmsTemplatesEnum.values()) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("value", c.getTitle() + "");
            map.put("label", c.getDesc());
            list.add(map);
        }
        return list;
    }
}
