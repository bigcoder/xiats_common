package com.cloud.common.model.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//货币类型
public enum WithdrawalBindTypeEnum {
    WEIXINPAY("wxpay","微信"),
    ALIPAY("alipay","支付宝"),
    ;
    private String name;
    private String desc;

    WithdrawalBindTypeEnum(String name, String desc) {
        this.name = name;
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public static WithdrawalBindTypeEnum getEnumDesc(String desc) {
        for (WithdrawalBindTypeEnum c : WithdrawalBindTypeEnum.values()) {
            if (c.getDesc().equals(desc)) {
                return c;
            }
        }
        return null;
    }

    public static WithdrawalBindTypeEnum getEnumCode(String name) {
        for (WithdrawalBindTypeEnum c : WithdrawalBindTypeEnum.values()) {
            if (c.getName().equals(name)) {
                return c;
            }
        }
        return null;
    }

    public static List<Map<String, String>> getAllEnum() {
        List<Map<String, String>> list = new ArrayList<>();
        for (WithdrawalBindTypeEnum c : WithdrawalBindTypeEnum.values()) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("value", c.getName() + "");
            map.put("label", c.getDesc());
            list.add(map);
        }
        return list;
    }
}
