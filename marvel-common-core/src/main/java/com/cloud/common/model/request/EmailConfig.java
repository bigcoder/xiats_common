package com.cloud.common.model.request;

import lombok.Data;

@Data
public class EmailConfig {

    /**
     * SMTP服务器域名
     */
    private String host;
    /**
     * SMTP服务端口
     */
    private Integer port;
    /**
     * 是否需要用户名密码验证
     */
    private Boolean auth = true;
    /**
     * 用户名
     */
    private String user;
    /**
     * 密码
     */
    private String pass;
    /**
     * 发送方，遵循RFC-822标准
     */
    private String from;
    /**
     * 是否HTML格式
     */
    private Boolean isHtml = false;
    //这里会返回发送的具体内容，返回值
    private String sendContent;
}
