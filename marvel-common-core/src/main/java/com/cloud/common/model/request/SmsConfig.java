package com.cloud.common.model.request;

import lombok.Data;

@Data
public class SmsConfig {
    /**
     * 私钥
     * */
    private String smsAccessKeyId;
    /**
     * 私钥
     * */
    private String smsSecret;
    /**
     * 签名
     * */
    private String smsSignName;

    /**
     * 发送的内容，返回值
     * */
    private String sendContent;
}
