package com.cloud.common.oss;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Data
@NoArgsConstructor
@Component
@Configuration
public class AliOssRequest {
    @Value("${oss.endpoint:''}")
    private String endpoint;
    @Value("${oss.accessKeyId:''}")
    private String accessKeyId;
    @Value("${oss.accessKeySecret:''}")
    private String accessKeySecret;
    @Value("${oss.bucketName:''}")
    private String bucketName;
    @Value("${imgUrl:''}")
    private String imgUrl;
}
