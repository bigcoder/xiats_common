package com.cloud.common.oss;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

@Data
public class AliOssResponse {
    private String path;
    private String absolutePath;
}
