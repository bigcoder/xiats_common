package com.cloud.common.oss;

import lombok.Data;

import java.io.InputStream;

@Data
public class AliOssUploadRequest {
    private String filedir;
    private InputStream file;
}
