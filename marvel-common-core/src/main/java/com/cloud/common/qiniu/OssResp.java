package com.cloud.common.qiniu;

/**
 * @Name OssResp
 * @Description TODO
 * @Author douhaichao
 * @Date 2018/12/4 14:08
 * @Version 1.0
 **/
public class OssResp {
    /**
     * 文件名称
     */
    private String file_name;
    /**
     * 文件域名
     */
    private String domain;
    /**
     * 文件相对路径
     */
    private String file_path;
    /**
     * 文件绝对路径
     */
    private String file_absolute_path;

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public String getFile_absolute_path() {
        return file_absolute_path;
    }

    public void setFile_absolute_path(String file_absolute_path) {
        this.file_absolute_path = file_absolute_path;
    }
}

