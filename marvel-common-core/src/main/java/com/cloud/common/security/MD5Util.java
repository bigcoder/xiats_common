package com.cloud.common.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;

public class MD5Util {
    private static final Logger logger = LoggerFactory.getLogger(MD5Util.class);
	/**
	 * 字符串返回MD5值
	 * @param s 字符串
	 * @return
	 */
	public final static String MD5(String s) {
        return MD5(s.getBytes());
    }
	
	/**
	 * 字符串返回MD5值
	 * @param btInput 字节数组
	 * @return
	 */
	public final static String MD5(byte[] btInput){
		char hexDigits[]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};       
        try {
            // 获得MD5摘要算法的 MessageDigest 对象
            MessageDigest mdInst = MessageDigest.getInstance("MD5");
            // 使用指定的字节更新摘要
            mdInst.update(btInput);
            // 获得密文
            byte[] md = mdInst.digest();
            // 把密文转换成十六进制的字符串形式
            int j = md.length;
            char str[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(str);
        } catch (Exception e) {
        	logger.error(e.getMessage());
            return null;
        }
	}
}
