package com.cloud.common.security;

import cn.hutool.core.codec.Base64Encoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SHA256 {
	private static final Logger logger = LoggerFactory.getLogger(SHA256.class);
    /**
     * sha256 加密
     *
     * @param val
     * @return
     * @throws NoSuchAlgorithmException
     */
    public static String getSHA256(String val) {
        try {
        	MessageDigest md5 = MessageDigest.getInstance("SHA-256");
        	 //加密后的字符串
        	return Base64Encoder.encode(md5.digest(val.getBytes("utf-8")));
        } catch (Exception e) {
        	logger.error(e.getMessage());
        }
        return null;
    }
}


