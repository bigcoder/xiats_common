package com.cloud.common.util;

/**
 * @author douhaichao
 * @since 2019/3/29 11:24
 */
public class AjaxResultUtils {

    /**
     * 返回结果
     * @param flag
     * @return
     */
    public static R getResult(Boolean flag){
        if (flag) {
            return new R<>(HttpReturnRnums.Success);
        }
        return new R<>(HttpReturnRnums.Failure);
    }

    /**
     * 参数错误
     * @return
     */
    public static R getParaError(){
        return new R<>(HttpReturnRnums.ParaError);
    }
}
