package com.cloud.common.util;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @Name DateTimeUtils
 * @Description TODO
 * @Author douhaichao
 * @Date 2019/4/3 11:44
 * @Version 1.0
 **/
public class DateTimeUtils {
    public static LocalDateTime parseStringToDateTime(String time, String format) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern(format);
        return LocalDateTime.parse(time, df);
    }

    public static LocalDateTime parseStringToDateTime(String time) {
        return parseStringToDateTime(time, "yyyy-MM-dd");
    }
}
