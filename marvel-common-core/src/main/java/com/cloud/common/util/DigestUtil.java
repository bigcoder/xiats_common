package com.cloud.common.util;

import lombok.extern.slf4j.Slf4j;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author yuaneg 2018/6/7
 */
@Slf4j
public class DigestUtil {

    /**
     * sha1加密
     */
    public static String SHA1(String value) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA1");
            byte[] bytes = messageDigest.digest(value.getBytes());
            return new BigInteger(1, bytes).toString(16);
        } catch (NoSuchAlgorithmException e) {
            log.error("sha1签名失败", e);
        }
        return null;
    }
}
