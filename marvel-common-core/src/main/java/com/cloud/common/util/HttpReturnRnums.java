package com.cloud.common.util;

/**
 * @Name HttpReturnRnums
 * @Description
 * @Author douhaichao
 * @Date 2021年3月1日 13:52:11
 * @Version 1.0
 **/
public enum HttpReturnRnums {
    /**
     * 成功
     */
    Success(0, "成功"),

    Failure(1, "失败"),

    /**
     * 系统错误
     */
    SystemError(-1, "系统错误"),
    /**
     * 参数错误
     */
    ParaError(-2, "参数错误"),

    FileNotExists(1000, "文件为空，请重新上传"),
    FileException(1001, "文件上传发生异常！"),
    FileMaxException(1002, "上传文件大小超过限制"),
    FileAttrException(1003, "上传文件扩展名是不允许的扩展名"),
    OldPasswordErrorException(2001, "旧密码输入错误"),

    EXISTSORDER(3000, "当前订单已存在"),

    NotLogin(10000, "当前用户未登录，请登录后操作"),;
    private int value = 0;
    private String desc;

    private HttpReturnRnums(int value, String desc) { // 必须是private的，否则编译错误
        this.value = value;
        this.desc = desc;
    }

    public int value() {
        return this.value;
    }

    public String desc() {
        return this.desc;
    }

    public static HttpReturnRnums getEnum(int index) {
        for (HttpReturnRnums c : HttpReturnRnums.values()) {
            if (c.value() == index) {
                return c;
            }
        }
        return null;
    }
}
