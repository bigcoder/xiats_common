package com.cloud.common.util;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.cloud.common.model.entity.EmailTemplates;
import com.cloud.common.model.entity.SmsTemplates;
import com.cloud.common.model.enums.SmsTemplatesEnum;
import com.cloud.common.model.request.SmsConfig;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 短信工具
 */
@Component
@AllArgsConstructor
public class SmsUtil {
    public boolean sendSms(SmsConfig smsConfig, String bcSmsTemplateStr, SmsTemplatesEnum smsTemplatesEnum,
                           String toMobile, JSONObject toValue) {
        try {
            if (StrUtil.isEmpty(toMobile)) {
                return false;
            }
            List<SmsTemplates> smsTemplatesList = JSONUtil.toList(bcSmsTemplateStr, SmsTemplates.class);

            SmsTemplates smsTemplates = smsTemplatesList.stream().
                    filter(t -> t.getAlias().equals(smsTemplatesEnum.getTitle())).
                    findFirst().orElse(new SmsTemplates());

            DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou",
                    smsConfig.getSmsAccessKeyId(), smsConfig.getSmsSecret());
            IAcsClient client = new DefaultAcsClient(profile);

            String TemplateParam = JSONUtil.toJsonStr(toValue);

            CommonRequest request = new CommonRequest();
            request.setSysMethod(MethodType.POST);
            request.setSysDomain("dysmsapi.aliyuncs.com");
            request.setSysVersion("2017-05-25");
            request.setSysAction("SendSms");
            request.putQueryParameter("RegionId", "cn-hangzhou");
            request.putQueryParameter("PhoneNumbers", toMobile);
            request.putQueryParameter("SignName", smsConfig.getSmsSignName());
            request.putQueryParameter("TemplateCode", smsTemplates.getTempId());
            request.putQueryParameter("TemplateParam", TemplateParam);

            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());

            String content = smsTemplates.getValue();
            //提取参数
            if (null != toValue) {
                Set<Map.Entry<String, Object>> entries = toValue.entrySet();
                for (Map.Entry<String, Object> item : entries) {
                    String key = item.getKey();
                    String value = Convert.toStr(item.getValue());
                    content = content.replace("{" + key + "}", value);
                }
            }
            smsConfig.setSendContent(content);
            return true;
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return false;
    }
}
