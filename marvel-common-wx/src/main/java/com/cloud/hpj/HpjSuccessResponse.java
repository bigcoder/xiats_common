package com.cloud.hpj;

import lombok.Data;

@Data
public class HpjSuccessResponse {
    private String trade_order_id;//	商户订单号	string(32)	支付时请求的商户订单号
    private String total_fee;//	订单支付金额	decimal(18,2)	订单支付金额
    private String transaction_id;//	交易号	string(32)	支付平台内部订单号
    private String open_order_id;//	虎皮椒内部订单号	string(32)	虎皮椒内部订单号
    private String order_title;//	订单标题	string(32)	订单标题
    private String status;//	订单状态	string(2)	目前固定值为：OD
    private String plugins;//	插件ID	string(32)	当传入此参数时才会有返回
    private String appid;//	支付渠道ID	string(32)
    private String time;//	时间戳	string(16)
    private String nonce_str;//	随即字符串	string(16)
    private String hash;//	签名	string(32)	请参考支付时签名算法
}
