package com.cloud.model;

import lombok.Data;

@Data
public class HpjPayResponse {
    private String openid;
    private String url_qrcode;
    private String url;
    private String errcode;
    private String errmsg;
    private String hash;
}
