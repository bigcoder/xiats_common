package com.cloud.model;

import lombok.Data;

@Data
public class HpjQueryPayStatusResponse {
    private String status;
    private String open_order_id;
}
