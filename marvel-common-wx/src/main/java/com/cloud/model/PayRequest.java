package com.cloud.model;

import lombok.Data;

@Data
public class PayRequest {
    private String appid;
    private String appsecret;
    /**
     * 单号
     */
    private String tradeOrderId;
    /**
     * 支付总金额
     */
    private String payAmount;
    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 可选。用户取消支付后，我们可能引导用户跳转到这个网址上重新进行支付
     */
    private String callbackUrl;

    /**
     * 必填。用户支付成功后，我们服务器会主动发送一个post消息到这个网址(注意：当前接口内，SESSION内容无效)
     */
    private String notifyUrl;

    /**
     * 可选。用户支付成功后，我们会让用户浏览器自动跳转到这个网址
     */
    private String returnUrl;

    /**
     * 备注
     */
    private String memo;

    /**
     * 1 会员充值 ，2购买礼物
     * */
    private Integer paymentType;

}
