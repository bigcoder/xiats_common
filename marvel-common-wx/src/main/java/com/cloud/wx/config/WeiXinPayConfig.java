package com.cloud.wx.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "wx.pay")
public class WeiXinPayConfig {
    private String appid;
    private String mchid;
    private String key;
    private String certPath;
    private String notifyUrl;
}
