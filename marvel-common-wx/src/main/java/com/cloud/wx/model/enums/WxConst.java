package com.cloud.wx.model.enums;

public class WxConst {
    public static String wxServiceUrl="https://api.weixin.qq.com/";

    public static String wxOAuthUrl="https://open.weixin.qq.com/connect/oauth2/authorize?appid={appid}&redirect_uri={redirect_uri}&response_type=code&scope=snsapi_userinfo&state=";

}
