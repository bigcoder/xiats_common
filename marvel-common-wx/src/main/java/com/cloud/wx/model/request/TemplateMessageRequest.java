package com.cloud.wx.model.request;

import lombok.Data;

@Data
public class TemplateMessageRequest {
    private String touser;
    private String template_id;
    private String url;
    private String miniprogram;
    private String data;
}
