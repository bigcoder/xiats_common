package com.cloud.wx.model.response;

import lombok.Data;

@Data
public class AccessTokenResponse {
    private String access_token;
    private Integer expires_in;
    private String refresh_token;
    private String openid;
    private String scope;
   
}
