package com.cloud.wx.model.response;

import lombok.Data;

@Data
public class TemplateMessageResponse {
    private String errcode;
    private String errmsg;
    private String msgid;
}
