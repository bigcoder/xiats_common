package com.cloud.wx.model.response;

import lombok.Data;

@Data
public class TokenResponse {
    private String access_token;
    private Integer expires_in;
}
