package com.cloud.wx.model.response;

import lombok.Data;

@Data
public class WeixinShareResponse {
    private String wxNoncestr;
    private String wxTimestamp;
    private String wxSignature;
    private String appId;
    private String wxUrl;
}
