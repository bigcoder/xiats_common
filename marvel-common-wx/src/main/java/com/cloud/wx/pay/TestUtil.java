package com.cloud.wx.pay;

import cn.hutool.http.HttpUtil;
import com.cloud.wx.config.WeiXinPayConfig;
import lombok.SneakyThrows;

import java.util.HashMap;
import java.util.Map;

public class TestUtil {
//调用方法
//    TestUtil testUtil = new TestUtil();
//    String sandboxSignKey = testUtil.getSandboxSignKey(wxPayConfig);

    @SneakyThrows
    public String getSandboxSignKey(WXPayConfig wxPayConfig) {

        WXPay wxPay = new WXPay(wxPayConfig);
        try {
            Map<String, String> params = new HashMap<String, String>();
            params.put("mch_id", wxPayConfig.getMchID());
            params.put("nonce_str", WXPayUtil.generateNonceStr());
            params.put("sign", WXPayUtil.generateSignature(params, wxPayConfig.getKey()));
            String strXML = wxPay.requestWithoutCert("/sandboxnew/pay/getsignkey",
                    params, wxPayConfig.getHttpConnectTimeoutMs(), wxPayConfig.getHttpReadTimeoutMs());

            Map<String, String> result = WXPayUtil.xmlToMap(strXML);
            System.out.println("retrieveSandboxSignKey:" + result);
            if ("SUCCESS".equals(result.get("return_code"))) {
                return result.get("sandbox_signkey");
            }
            return null;
        } catch (Exception e) {
            System.out.println("获取sandbox_signkey异常" + e.getMessage());
            return null;
        }
    }
}
