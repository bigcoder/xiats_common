package com.cloud.wx.template;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.text.UnicodeUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.cloud.redis.config.RedisUtils;
import com.cloud.wx.config.WxConfig;
import com.cloud.wx.model.enums.RedisKey;
import com.cloud.wx.model.enums.WxConst;
import com.cloud.wx.model.request.TemplateData;
import com.cloud.wx.model.request.TemplateMessageRequest;
import com.cloud.wx.model.response.AccessTokenResponse;
import com.cloud.wx.model.response.TemplateMessageResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.TimeUnit;

//模板消息 接口
//source:https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Template_Message_Interface.html#0
@AllArgsConstructor
@Service
public class WxMessageService {
    private final WxConfig wxConfig;
    private final RedisUtils redisUtils;
    private static String templateDomain = WxConst.wxServiceUrl + "cgi-bin/template/";

    //0 获取accessToken
    public String getAccessToken() {
        String key = RedisKey.messageTokenKey;
        Object o = redisUtils.get(key);
        if (o == null) {
            String url = WxConst.wxServiceUrl + "cgi-bin/token?grant_type=client_credential&appid=" + wxConfig.getAppid() + "&secret=" + wxConfig.getAppsecret();
            String s = HttpUtil.get(url);
            AccessTokenResponse accessTokenResponse = JSONUtil.toBean(s, AccessTokenResponse.class);
            String access_token = accessTokenResponse.getAccess_token();
            redisUtils.set(key, access_token, 1, TimeUnit.HOURS);
            return access_token;
        }
        return Convert.toStr(o);
    }

    //1 设置所属行业
    public boolean setTemplateIndustry() {
        String url = templateDomain + "api_add_template?access_token=" + getAccessToken();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("industry_id1", 2);
        jsonObject.put("industry_id2", 4);
        String s = HttpUtil.post(url, jsonObject.toString());
        return s.contains("first_class");
    }

    //2 获取设置的行业信息
    public String getTemplateIndustry() {
        String url = templateDomain + "get_industry?access_token=" + getAccessToken();
        String s = HttpUtil.get(url);
        return s;
    }

    //
    //3 获得模板ID,批量设置
    public boolean setTemplateId(List<String> list) {
        for (String item : list) {
            String url = templateDomain + "api_add_template?access_token=" + getAccessToken();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("template_id_short", item);
            String s = HttpUtil.post(url, jsonObject.toString());
        }
        return true;
    }

    //4 获取模板列表
    public String getTemplateList(List<String> list) {
        String url = templateDomain + "get_all_private_template?access_token=" + getAccessToken();
        String s = HttpUtil.get(url);
        return s;
    }

    //5 删除模板,公众帐号下模板消息ID
    public boolean removeTemplateId(List<String> list) {
        for (String item : list) {
            String url = templateDomain + "del_private_template?access_token=" + getAccessToken();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("template_id", item);
            String s = HttpUtil.post(url, jsonObject.toString());
        }
        return true;
    }

    //6 发送模板消息
    public TemplateMessageResponse sendTemplateMessage(TemplateMessageRequest request) {
        String url = templateDomain.replace("template/", "message/template/") + "send?access_token=" + getAccessToken();
        String req = JSONUtil.toJsonStr(request);
        String reqStr = UnicodeUtil.toString(req);
        if (StrUtil.isEmpty(reqStr)) {
            return null;
        }
//        reqStr = reqStr.replace("\\", "");
        String s = HttpUtil.post(url, reqStr);
        TemplateMessageResponse templateMessageResponse = JSONUtil.toBean(s, TemplateMessageResponse.class);
        return templateMessageResponse;
    }

    public TemplateMessageResponse sendTemplateMessageData(String reqStr) {
        	/* 新订单提醒
		 *
			{{first.DATA}}
			内容：{{keyword1.DATA}}
			时间：{{keyword2.DATA}}
			{{remark.DATA}}
		 */
        String url = templateDomain.replace("template/", "message/template/") + "send?access_token=" + getAccessToken();
//        String req = JSONUtil.toJsonStr(request);
//        String reqStr = UnicodeUtil.toString(req);
//        if (StrUtil.isEmpty(reqStr)) {
//            return null;
//        }
//        reqStr = reqStr.replace("\\", "");
        String s = HttpUtil.post(url, reqStr);
        TemplateMessageResponse templateMessageResponse = JSONUtil.toBean(s, TemplateMessageResponse.class);
        return templateMessageResponse;
    }

    //7 事件推送

}
