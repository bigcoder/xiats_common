package com.cloud.wx.util;

import com.cloud.wx.config.WxConfig;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class WxCheckSignatureService {
   private final WxConfig wxConfig;
    /**
     * @Description  进行签名认证
     * @author lst
     * @date 2020-8-20 10:49
     * @param signature 微信加密签名
     * @param timestamp 时间戳
     * @param nonce 随机数
     * @param echostr 随机字符串
     * @return java.lang.String
     */
    public String checkSignature(String signature, String timestamp, String nonce, String echostr) {
        // 1.将token、timestamp、nonce三个参数进行字典序排序
//        log.info("signature:{},token:{},timestamp:{},nonce:{}",signature,token,timestamp,nonce);
        String tmpStr = ShaUtil.getSHA1(wxConfig.getToken(),  timestamp,  nonce);
        //TODO 进行对比
//        log.info("随机字符串echostr:{}",echostr);
//        log.info("tmpStr:{}",tmpStr);
        if (tmpStr.equals(signature.toUpperCase())) {
            return echostr;
        }
        return null;
    }
}
