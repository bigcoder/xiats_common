package com.cloud.wx.util;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.cloud.wx.model.enums.WxConst;
import com.cloud.wx.model.response.AccessTokenResponse;
import com.cloud.wx.model.response.SnsapiUserinfoResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class WxUserinfoService {
    //    第四步：拉取用户信息(需scope为 snsapi_userinfo)
    public SnsapiUserinfoResponse getUserinfo(AccessTokenResponse req) {
        String url = WxConst.wxServiceUrl + "sns/userinfo?access_token="+req.getAccess_token()+"&openid="+req.getOpenid()+"&lang=zh_CN";
        String s = HttpUtil.get(url);
        SnsapiUserinfoResponse snsapiUserinfoResponse = JSONUtil.toBean(s, SnsapiUserinfoResponse.class);
        return snsapiUserinfoResponse;
    }
}
